﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Timers;
using rtspServer.Models;

namespace rtspServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RtspController : Controller
    {
        private readonly IWebHostEnvironment Environment;

        private static Timer _timer;

        private static object _locker = new Object();

        public RtspController(IWebHostEnvironment hosting)
        {
            Environment = hosting;

            if (_timer == null)
            {
                _timer = new Timer();
                _timer.Elapsed += (sender, args) => Check();
                _timer.Interval = 10000;
                _timer.AutoReset = true;
                _timer.Start();
            }
        }

        private void Check()
        {
            lock (_locker)
            {
                using (var context = new DataContext())
                {
                    var DbProcesses = context.RtspProcesses.ToList();
                    DbProcesses = DbProcesses.Where(x => x.LastUpdate < DateTime.Now.AddMinutes(-1)).ToList();
                    var processes = Process.GetProcesses();

                    foreach (var _process in DbProcesses)
                    {
                        var process = processes.FirstOrDefault(x => x.Id == _process.ProcessId);
                        var folder = HttpUtility.UrlEncode(_process.Rtsp);

                        if (process == null)
                        {
                            context.RtspProcesses.Remove(_process);
                            context.SaveChanges();

                            if (Directory.Exists($"Stream/{folder}"))
                                Directory.Delete($"Stream/{folder}", true);

                            continue;
                        }

                        process.Kill();
                        process.Dispose();

                        context.RtspProcesses.Remove(_process);
                        context.SaveChanges();

                        if (Directory.Exists($"Stream/{folder}"))
                            Directory.Delete($"Stream/{folder}", true);
                    }
                }
            }
        }

        [HttpGet("{rtsp}/GetStream")]
        public IActionResult GetStream(string rtsp)
        {
            if (string.IsNullOrWhiteSpace(rtsp))
                return NotFound();

            if (rtsp.ToLower().Contains("%2f"))
                rtsp = HttpUtility.UrlDecode(rtsp);

            var encoded = HttpUtility.UrlEncode(rtsp);

            using (var context = new DataContext())
            {
                if (!context.RtspProcesses.Any(x => x.Rtsp.ToLower() == rtsp.ToLower()))
                {
                    Open(encoded, rtsp);
                }
                else
                {
                    var model = context.RtspProcesses.FirstOrDefault(x => x.Rtsp.ToLower() == rtsp.ToLower());

                    model.LastUpdate = DateTime.Now;

                    context.SaveChanges();
                }
            }

            if (!Directory.Exists($"Stream/{encoded}"))
                return NotFound();

            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");

            try
            {
                return File(System.IO.File.OpenRead(Path.Combine("Stream", encoded, "example.m3u8")), "application/octet-stream", true);
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }

        [HttpGet("{rtsp}/{partfile}")]
        public IActionResult GetStreamTS(string rtsp, string partfile)
        {
            if (rtsp.ToLower().Contains("%2f"))
                rtsp = HttpUtility.UrlDecode(rtsp);

            var encoded = HttpUtility.UrlEncode(rtsp);

            if (string.IsNullOrWhiteSpace(rtsp) || string.IsNullOrWhiteSpace(partfile))
                return NotFound();

            if (System.IO.File.Exists(Path.Combine("Stream", encoded, partfile)))
            {
                using (var context = new DataContext())
                {
                    var model = context.RtspProcesses.FirstOrDefault(x => x.Rtsp.ToLower() == rtsp.ToLower());

                    model.LastUpdate = DateTime.Now;

                    context.SaveChanges();
                }
            }
            else
            {
                return NotFound();
            }

            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");

            try
            {
                return File(System.IO.File.OpenRead(Path.Combine("Stream", encoded, partfile)), "application/octet-stream", true);
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }

        private void Open(string folder, string rtsp)
        {
            if (string.IsNullOrWhiteSpace(rtsp))
                return;

            using (var context = new DataContext())
            {
                if (context.RtspProcesses.Any(x => x.Rtsp.ToLower() == rtsp.ToLower()))
                    return;
            }

            rtsp = rtsp.Replace("%2F", "/");

            string args = $"-i {rtsp} " +
                            "-acodec copy " +
                            "-vcodec copy " +
                            "-f hls " +
                            "-hls_flags delete_segments+append_list " +
                            "-start_number 1 " +
                            "-hls_time 5  " +
                            "-hls_list_size 6 " +
                            "-hls_delete_threshold 1 " +
                            "-segment_time 5 " +
                            "-err_detect explode " +
                            $"Stream/{folder}/example.m3u8";

            if (!Directory.Exists($"Stream/{folder}"))
                Directory.CreateDirectory($"Stream/{folder}");

            if (!System.IO.File.Exists($"Stream/{folder}/example.m3u8"))
            {
                using (FileStream fs = System.IO.File.Create($"Stream/{folder}/example.m3u8"))
                {
                    fs.Close();
                }
            }

            var startInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(Environment.ContentRootPath, "ffmpeg.exe"),
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                Arguments = args,
                WorkingDirectory = Environment.ContentRootPath
            };

            var process = new Process { StartInfo = startInfo };

            process.Start();

            using (var context = new DataContext())
            {
                context.RtspProcesses.Add(new RtspProcess
                {
                    ProcessId = process.Id,
                    Rtsp = rtsp,
                    LastUpdate = DateTime.Now
                });

                context.SaveChanges();
            }
        }
    }
}
