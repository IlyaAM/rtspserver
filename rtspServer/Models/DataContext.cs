﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rtspServer.Models
{
    public class DataContext : DbContext
    {
        public DbSet<RtspProcess> RtspProcesses { get; set; }
        public DataContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=rtsp_server;Username=postgres;Password=AskAlexandria;");
        }
    }
}
