﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rtspServer.Models
{
    public class RtspProcess
    {
        public int Id { get; set; }
        public int ProcessId { get; set; }
        public string Rtsp { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
